# Example of a local configuration with JDK7 on Windows

# Be careful with spaces!
ifeq (${MSYSTEM},)
base := C:/Program Files/Java/jdk1.7.0_79/bin
else
base := /c/Program Files/Java/jdk1.7.0_79/bin
endif
JAVA      := "${base}/java"
JAVAC     := "${base}/javac"
KEYTOOL   := "${base}/keytool"
JARSIGNER := "${base}/jarsigner"

# Assuming the android sdk is in there
ifeq (${MSYSTEM},)
base    := D:/win32/program-files/android-sdk-windows
else
base    := /d/win32/program-files/android-sdk-windows
endif
version := 23.0.3
api     := android-23
DEX_EXT := .bat
AAPT        := "${base}/build-tools/${version}/aapt"
DEX         := "${base}/build-tools/${version}/dx${DEX_EXT}"
ZIPALIGN    := "${base}/build-tools/${version}/zipalign"
ANDROID_JAR := "${base}/platforms/${api}/android.jar"
ADB         := "${base}/platform-tools/adb"
