package com.ex;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class Ejemplo0Activity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ejemplo0);

		Spinner dlength_input = (Spinner)findViewById(R.id.dlength);
		String dlength_choices[] = {
			"1 dígito",
			"2 dígitos",
			"3 dígitos",
			"4 dígitos"
		};
		ArrayAdapter<String> dlength_adapter =
			new ArrayAdapter<String>(
				this,
				android.R.layout.simple_spinner_item,
				dlength_choices);
			dlength_adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item
			);
		dlength_input.setAdapter(dlength_adapter);
		dlength_input.setPrompt("Dígitos");

		Spinner speed_input = (Spinner)findViewById(R.id.speed);
		String speed_choices[] = {
			"0.1 segundos",
			"0.3 segundos",
			"0.5 segundos",
			"0.7 segundos",
			"1 segundo",
			"1.5 segundos",
			"2 segundos",
			"3 segundos",
			"5 segundos"
		};
		ArrayAdapter<String> speed_adapter =
			new ArrayAdapter<String>(
				this,
				android.R.layout.simple_spinner_item,
				speed_choices);
			speed_adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item
			);
		speed_input.setAdapter(speed_adapter);
		speed_input.setPrompt("Velocidad");

		Spinner size_input = (Spinner)findViewById(R.id.size);
		String size_choices[] = {
			"5 números",
			"10 números",
			"15 números",
			"20 números",
			"30 números",
			"50 números",
			"100 números"
		};
		ArrayAdapter<String> size_adapter =
			new ArrayAdapter<String>(
				this,
				android.R.layout.simple_spinner_item,
				size_choices);
			size_adapter.setDropDownViewResource(
				android.R.layout.simple_spinner_dropdown_item
			);
		size_input.setAdapter(size_adapter);
		size_input.setPrompt("Números");

		Button start_button = (Button)findViewById(R.id.start);
			start_button.setText("Comenzar");

	}

}
