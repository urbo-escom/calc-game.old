package com.ex;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Spinner;

public class Ejemplo0CheckActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ejemplo0_check);

		EditText input = (EditText)findViewById(R.id.input);
			input.setHint("Suma total");

		TextView output = (TextView)findViewById(R.id.output);
			output.setText("0");

		TextView status = (TextView)findViewById(R.id.status);
			status.setText("Esperando");

		Button check_button = (Button)findViewById(R.id.check);
			check_button.setText("Comprobar");

		Button retry_button = (Button)findViewById(R.id.retry);
			retry_button.setText("Nuevo");

		Button stop_button = (Button)findViewById(R.id.stop);
			stop_button.setText("Detener");

	}

}
