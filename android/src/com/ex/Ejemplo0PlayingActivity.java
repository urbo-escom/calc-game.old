package com.ex;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Spinner;

public class Ejemplo0PlayingActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ejemplo0_playing);

		TextView current_num = (TextView)findViewById(R.id.current_num);
			current_num.setText("0");

		ProgressBar progress = (ProgressBar)findViewById(R.id.progress);
			progress.setMax(100);
			progress.setProgress(30);

		Button stop_button = (Button)findViewById(R.id.stop);
			stop_button.setText("Detener");

	}

}
