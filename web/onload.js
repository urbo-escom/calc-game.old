function onload_register(func) {
	var onload = window.onload;
	if (typeof onload == 'function') {
		window.onload = function onload() {
			onload();
			func();
		}
	} else {
		window.onload = func;
	}
}

var Digits = {};
Digits.min = function min(size) {
	return Math.pow(10, size - 1);
};
Digits.max = function max(size) {
	return Math.pow(10, size) - 1;
};
Digits.rand = function rand(size) {
	var range = Digits.max(size) - Digits.min(size) + 1;
	return Math.floor(Math.random()*range) + Digits.min(size);
}
