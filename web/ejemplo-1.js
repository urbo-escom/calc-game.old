onload_register(function() {

	var dlength_elem = document.getElementById('ex1-digit-length');
	var time_elem = document.getElementById('ex1-time');
	var optype_elem = document.getElementById('ex1-optype');

	var dlength = 0;
	var time = 0;
	var optype = '+';
	var extract_vals = function extract_vals() {
		dlength = parseInt(dlength_elem.value + '', 10);
		time = parseInt(time_elem.value + '', 10);
		optype = optype_elem.value + '';
	}

	var form = document.ex1;
	var options = document.getElementById('ex1-options');
	var outputs = document.getElementById('ex1-outputs');
	var results = document.getElementById('ex1-results');

	var run = document.getElementById('ex1-run');
	var stop = document.getElementById('ex1-stop');
	var check = document.getElementById('ex1-check');
	var retry = document.getElementById('ex1-retry');

	var op0 = document.getElementById('ex1-operand0');
	var op1 = document.getElementById('ex1-operand1');
	var op_ = document.getElementById('ex1-operation');
	var result = document.getElementById('ex1-result');
	var sum = document.getElementById('ex1-sum');
	var status = document.getElementById('ex1-status');
	var stat_score = document.getElementById('ex1-stat-score');
	var stat_time = document.getElementById('ex1-stat-time');

	var timer = new Chron();

	var disable = function(elem) {
		elem.disabled = true;
		return elem;
	}
	var enable = function(elem) {
		elem.disabled = false;
		return elem;
	}
	var hide = function(elem) {
		elem.className = "hidden";
		return elem;
	}
	var unhide = function(elem) {
		elem.className = "";
		return elem;
	}

	var state = {

		checking: function() {

			hide(options);
			disable(dlength_elem);
			disable(time_elem);
			disable(optype_elem);

			unhide(outputs);
			unhide(results);
			enable(result);
			disable(sum);

			hide(disable(run));
			unhide(enable(stop));
			unhide(enable(check));
			unhide(enable(retry));

			result.focus();

		},

		waiting: function() {

			hide(options);
			disable(dlength_elem);
			disable(time_elem);
			disable(optype_elem);

			unhide(outputs);
			unhide(results);
			disable(result);
			disable(sum);

			hide(disable(run));
			unhide(enable(stop));
			hide(disable(check));
			unhide(enable(retry));

		},

		stopped: function() {

			unhide(options);
			enable(dlength_elem);
			enable(time_elem);
			enable(optype_elem);

			hide(outputs);
			hide(results);
			disable(result);
			disable(sum);

			unhide(enable(run));
			hide(disable(stop));
			hide(disable(check));
			hide(disable(retry));

			dlength_elem.focus();

		}

	}

	run.onclick = function() {
		if (timer.running)
			return false;

		extract_vals();
		console.log('ex1-run', dlength, time, optype);

		form.onsubmit = function() {};
		op0.value = '???';
		op1.value = '???';
		op_.value = optype;
		status.value = '';

		var total = 0;
		var good_ones = 0;
		var a;
		var b;
		var r;

		sum.value = '';
		var next = function next() {
			a = Digits.rand(dlength);
			b = Digits.rand(dlength);
			if (optype == '-' && a < b) {
				var t = a;
				a = b;
				b = t;
			}
			if (optype == '+')
				r = a + b;
			else
				r = a - b;
			console.log('ex1-sum', a, optype, b, r);
			op0.value = a;
			op1.value = b;
			op_.value = optype;
			result.value = '';
			result.focus();
			state.checking();
			check.onclick = function() {
				var n = parseInt(result.value + '', 10);
				console.log('ex1-check', r, n);
				var str = a + optype + b + ' = ' + r;
				if (r != n) {
					str = 'NO ' + n + ' != ' + str;
					status.value = str;
					status.className = "bad";
				} else {
					str = 'OK ' + n + ' = ' + str;
					status.value = str;
					status.className = "good";
					good_ones++;
				}
				total++;
				sum.value = r;
				next();
				return false;
			};
			form.onsubmit = check.onclick;
			retry.onclick = run.onclick;
		};

		status.className = "";
		result.value = '';
		result.placeholder = 'Supr-->     ';
		result.oninput = function() {
			var n = parseInt(result.value + '', 10);
			console.log('ex1-check-oninput', r, n);
			result.setSelectionRange(0, 0);
			if (r == n)
				check.onclick();
		};

		var started = false;
		var wait = 1000;
		timer.sleep = 100;
		timer.on('timer', function(elapsed) {
			stat_score.value = good_ones + '/' + total;
			var sec = time + wait - elapsed;
			if (sec < 0) {
				sign = '-';
				sec = -sec;
			} else {
				sign = '';
			}
			var msec = sec%1000;
			sec = (sec - msec)/1000;
			sec = sign + sec;
			if (msec == 0)
				sec += '.000';
			else if (msec < 100)
				sec += '.0' + msec;
			else
				sec += '.' + msec;
			stat_time.value = sec + ' seg';
			if (elapsed < 0) {
				console.log('ex1-error elapsed', elapsed);
				timer.stop();
				state.stopped();
			} else if (wait < elapsed && elapsed < time + wait) {
				if (started)
					return;
				started = true;
				next();
			} else if (time + wait < elapsed) {
				timer.stop();
				state.waiting();
				form.onsubmit = check.onclick = state.stopped;
			}
		});
		timer.stop();
		timer.run();
		state.waiting();
		return false;
	};

	stop.onclick = function() {
		timer.stop();
		state.stopped();
		form.onsubmit = run.onclick;
		result.oninput = function() {};
	};
	stop.onclick();

});
