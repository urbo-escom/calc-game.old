//
// Timer or  cronómetro implementado usando  setTimeout, no es  ni intenta
// ser  un cronómetro  preciso, los  timers en  JavaScript pueden  ser tan
// imprecisos como quieran, sobretodo cuando el navegador tiene tareas más
// importantes o la pestaña del navegador no está siendo enfocada.
//
//         - run()
//           Inicia o continúa  una cuenta, a menos  que el cronómetro
//           se detenga mediante pause()  o stop(), sucesivas llamadas
//           a esta función son ignoradas.
//
//         - pause()
//           Pausa el cronómetro sin borrar la cuenta.
//
//         - reset()
//           Restaura la cuenta a un estado limpio, en ceros.
//
//         - stop()
//           Detiene  el cronómetro,  primero lo  pausa con  pause() y
//           luego lo restaura con reset().
//
var Chron = function chron_create(opts) {

	var o = opts || {};
	var thiz = {};

	var sleep   = o.sleep   || 100;
	var ontimer = o.ontimer || function() {};
	var onrun   = o.onrun   || function() {};
	var onpause = o.onpause || function() {};
	var onreset = o.onreset || function() {};
	var onstop  = o.onstop  || function() {};
	var onerror = o.onerror;

	var running = false;
	var start    = 0;
	var freezed  = 0;
	var expected = 0;
	var elapsed  = 0;

	var new_sleep = sleep;

	Object.defineProperty(thiz, 'running', {
		get: function() {
			return running && true;
		},
		enumerable:   true,
		configurable: false
	});
	Object.defineProperty(thiz, 'sleep', {
		get: function() {
			return sleep;
		},
		set: function(val) {
			new_sleep = val;
		},
		enumerable:   true,
		configurable: false
	});
	Object.defineProperty(thiz, 'started', {
		get: function() {
			return start;
		},
		enumerable:   true,
		configurable: false
	});
	Object.defineProperty(thiz, 'paused', {
		get: function() {
			return !running && freezed != 0;
		},
		enumerable:   true,
		configurable: false
	});
	Object.defineProperty(thiz, 'elapsed', {
		get: function() {
			return Date.now() - start;
		},
		enumerable:   true,
		configurable: false
	});

	var init = function init() {
		start    = Date.now();
		freezed  = 0;
		expected = 0;
		elapsed  = 0;
	};
	var count = function count() {
		expected += sleep;
		elapsed   = Date.now() - start;
	};
	var err = function err() {
		return elapsed < 0;
	};
	var inform = function inform(f) {
		f(elapsed, expected, thiz);
	};
	var timer = function() {
		if (!running)
			return;
		count();
		if (err())
			inform(onerror || ontimer);
		else
			inform(ontimer);
		sleep = new_sleep;
		window.setTimeout(timer, sleep);
	};

	thiz.run = function run() {
		if (running)
			return;
		if (0 != freezed) {
			start += Date.now() - freezed;
			freezed = 0;
		}
		running = true;
		window.setTimeout(timer, sleep);
		inform(onrun);
	};

	thiz.pause = function pause() {
		running = false;
		freezed = Date.now();
		inform(onpause);
	};

	thiz.reset = function reset() {
		init();
		inform(onreset);
	};

	thiz.stop = function stop() {
		thiz.pause();
		thiz.reset();
		inform(onstop);
	};

	thiz.on = function on(name, func) {
		switch (name) {
		case 'timer': ontimer = func || function() {}; break;
		case 'run':   onstart = func || function() {}; break;
		case 'pause': onpause = func || function() {}; break;
		case 'reset': onreset = func || function() {}; break;
		case 'stop':  onstop  = func || function() {}; break;
		}
		return thiz;
	}

	return thiz;
};
