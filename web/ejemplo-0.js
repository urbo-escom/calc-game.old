onload_register(function() {

	var dlength_input = document.getElementById('ex0-digit-length');
	var speed_input = document.getElementById('ex0-speed');
	var size_input = document.getElementById('ex0-num-size');

	var dlength = 0;
	var speed = 0;
	var size = 0;
	var arr = [];
	var extract_vals = function extract_vals() {
		dlength = parseInt(dlength_input.value + '', 10);
		speed = parseInt(speed_input.value + '', 10);
		size = parseInt(size_input.value + '', 10);
		arr = [];
	}

	var form = document.ex0;
	var section_options = document.getElementById('ex0-options');
	var section_outputs = document.getElementById('ex0-outputs');
	var section_results = document.getElementById('ex0-results');

	var button_run = document.getElementById('ex0-run');
	var button_stop = document.getElementById('ex0-stop');
	var button_check = document.getElementById('ex0-check');
	var button_retry = document.getElementById('ex0-retry');

	var current_num_output = document.getElementById('ex0-current-num');
	var result_input = document.getElementById('ex0-result');
	var sum_output = document.getElementById('ex0-sum');
	var status_output = document.getElementById('ex0-status');

	var timer = new Chron();

	var disable = function(elem) {
		elem.disabled = true;
		return elem;
	};
	var enable = function(elem) {
		elem.disabled = false;
		return elem;
	};
	var hide = function(elem) {
		elem.className = "hidden";
		return elem;
	};
	var unhide = function(elem) {
		elem.className = "";
		return elem;
	};

	var hide_all = function hide_all() {

		hide(section_options);
		disable(dlength_input);
		disable(speed_input);
		disable(size_input);

		hide(section_outputs);
		hide(section_results);
		disable(result_input);
		disable(sum_output);

		hide(disable(button_run));
		hide(disable(button_stop));
		hide(disable(button_check));
		hide(disable(button_retry));

	};

	var state_table = {
		'idle':     ['start'],
		'starting': ['timetick', 'timeout', 'stop'],
		'playing':  ['next-value', 'finish', 'stop'],
		'checking': ['check', 'stop'],
		'checked':  ['retry', 'stop'],
	};

	var state = 'idle';
	var handle_event = function(event) {
		console.log(
			'current-state', state,
			'event', event
		);
		var action = state_table[state][event];
		if (typeof action !== 'function')
			throw new Error(
				'No action for state "' +
					state + '" ' +
				'and event "' +
					event + '"');
		action();
	};

	var stop = function stop() {
		state = 'idle';
		hide_all();
		unhide(section_options);
		enable(dlength_input);
		enable(speed_input);
		enable(size_input);
		dlength_input.focus();
		unhide(enable(button_run));
		timer.stop();
		form.onsubmit = function() {
			handle_event('start');
		};
	};
	stop();

	state_table['starting']['stop'] =
	state_table['playing']['stop'] =
	state_table['checking']['stop'] =
	state_table['checked']['stop'] =
		stop;

	state_table['idle']['start'] = function() {
		state = 'starting';
		hide_all();
		unhide(section_outputs);
		unhide(enable(button_stop));
		button_stop.focus();
		extract_vals();
		form.onsubmit = stop;
		current_num_output.value = '-';
		current_num_output.className = '';
		status_output.value = '';
		status_output.className = '';
		timer.stop();
		timer.sleep = 300;
		timer.on('timer', function(elapsed) {
			if (elapsed < 3000)
				handle_event('timetick');
			else
				handle_event('timeout');
		});
		timer.run();
	};

	state_table['starting']['timetick'] = function() {
		state = 'starting';
		console.log('starting', dlength, speed, size, timer.elapsed);
		var left = Math.floor(timer.elapsed/1000);
		if (left <= 3)
			left = 3 - left;
		else
			left = 0;
		current_num_output.value = '<<' + left + '>>';
	};

	state_table['starting']['timeout'] = function() {
		state = 'playing';
		current_num_output.value = 'GO';
		timer.stop();
		timer.sleep = 300;
		timer.on('timer', function(elapsed) {
			if (elapsed < 500) {
				console.log('sleep', elapsed);
			} else {
				handle_event('next-value');
			}
		});
		timer.run();
	};

	state_table['playing']['next-value'] = function() {
		state = 'playing';
		var val = Digits.rand(dlength);
		arr.push(val);
		size--;
		console.log('sum', arr.join(' + '));
		current_num_output.value = val;
		if (size%2)
			current_num_output.className = "output-even";
		else
			current_num_output.className = "output-odd";
		timer.stop();
		timer.sleep = speed/3 - 10;
		timer.on('timer', function(elapsed) {
			console.log('next-value', elapsed);
			if (elapsed < speed) {
				console.log('next-value elapsed', elapsed);
			} else if (size <= 0) {
				handle_event('finish');
			} else {
				handle_event('next-value');
			}
		});
		timer.run();
	};

	state_table['playing']['finish'] = function() {
		state = 'checking';
		result_input.value = '';
		sum_output.value = '';
		timer.stop();
		hide_all();
		unhide(section_results);
		unhide(enable(result_input));
		unhide(enable(sum_output));
		unhide(enable(button_check));
		unhide(enable(button_stop));
		unhide(enable(button_retry));
		result_input.focus();
		form.onsubmit = function() {
			handle_event('check');
		};
	};

	state_table['checking']['check'] = function() {
		state = 'checked';
		var n = parseInt(result_input.value + '', 10);
		disable(result_input);
		hide(disable(button_check));
		var arr_sum = arr.reduce(function(a, b) {
			return a + b;
		}, 0);
		var sum_str = arr.join(' + ');
		console.log('check', arr_sum, n);
		var str = arr_sum + ' = ' + sum_str;
		if (arr_sum != n) {
			str = 'NO ' + n + ' != ' + str;
			status_output.value = str;
			status_output.className = "bad";
		} else {
			str = 'OK ' + n + ' = ' + str;
			status_output.value = str;
			status_output.className = "good";
		}
		sum_output.value = arr_sum;
		current_num_output.className = '';
		form.onsubmit = function() {
			handle_event('retry');
		};
	};

	state_table['checking']['retry'] = state_table['idle']['start'];
	state_table['checked']['retry'] = state_table['idle']['start'];

	button_run.onclick = function() {
		handle_event('start');
		return false;
	};
	button_stop.onclick = function() {
		handle_event('stop');
		return false;
	};
	button_check.onclick = function() {
		handle_event('check');
		return false;
	};
	button_retry.onclick = function() {
		handle_event('retry');
		return false;
	};

});
