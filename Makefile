.PHONY: all
.PHONY: clean

root := $(patsubst %/,%,$(dir $(lastword ${MAKEFILE_LIST})))

include ${root}/mk/shell.mk
include ${root}/mk/util.mk
include ${root}/conf.mk

ptgt := android
tgt  := android

package    := com.ex
activities := $(wildcard ${root}/android/src/com/ex/*Activity.java)
activities := $(basename $(notdir ${activities}))
manifest   := ${root}/android/AndroidManifest.xml
resources  := ${root}/android/res
sources    := ${root}/android/src
$(if $(strip $V),$(info [${ptgt}] package    := ${package}),)
$(if $(strip $V),$(info [${ptgt}] activities := ${activities}),)
$(foreach tgt, ${tgt}/build, $(eval include ${root}/mk/android.mk))

clean: ${ptgt}/clean

${ptgt}/clean: tgt := ${tgt}/build
${ptgt}/clean: ${ptgt}/clean-files ${ptgt}/clean-dirs
	rm -rf ${tgt}/
${tgt}/build/: | ${tgt}/
