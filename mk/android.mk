#
# All of these are separated by spaces unless stated otherwise:
#
# ${package}         - application package
# ${activities}      - launchable activities
# ${manifest}        - manifest file (AndroidManifest.xml)
# ${resources}       - directories of resource files (layout.xml, etc.)
# ${sources}         - directories of source files (.java files)
# ${libraries}       - jar files
#
# Variables that may contain spaces (thery are used untouched):
#
# ${ANDROID_JAR}     - android jar (Android SDK)
#
# ${AAPT}            - Android Asset Packaging Tool (Android SDK)
# ${JAVAC}           - Java Compiler (JDK)
# ${DEX}             - Dalvik Executable tool (Android SDK)
# ${JARSIGNER}       - Jar signer (JDK)
# ${ZIPALIGN}        - Android zip aligner (Android SDK)
#
# ${CLASSPATH}       - This is  appended to the javac  classpath untouched. It
#                      is  meant  to contain  spaces  or  similar syntax  that
#                      avoids its manipulation by GNU Make
#


override JAVAC     ?= javac
override KEYTOOL   ?= keytool
override JARSIGNER ?= jarsigner

override AAPT      ?= aapt
override DEX       ?= dx
override ZIPALIGN  ?= zipalign
override ADB       ?= adb


resources_files := $(call rwildcard_file, ${resources})
sources_files   := $(call rwildcard_file, ${sources})

.PHONY: ${ptgt}/all
.PHONY: ${ptgt}/clean
.PHONY: ${ptgt}/install
.PHONY: ${ptgt}/uninstall
.PHONY: ${ptgt}/install-force
$(foreach activity, ${activities}, \
	$(eval .PHONY: ${ptgt}/start-${activity}) \
)

.PHONY: ${ptgt}/clean-files
.PHONY: ${ptgt}/clean-dirs
${ptgt}/all: ${tgt}/apk-align.apk
${ptgt}/clean-files: tgt := ${tgt}
${ptgt}/clean-dirs: tgt := ${tgt}
${ptgt}/clean-files:
	rm -f  ${tgt}/generated.log
	rm -f  ${tgt}/classes.log
	rm -f  ${tgt}/classes.dex
	rm -f  ${tgt}/debug.keystore
	rm -f  ${tgt}/apk-raw.apk
	rm -f  ${tgt}/apk-sign.apk
	rm -f  ${tgt}/apk-align.apk
${ptgt}/clean-dirs:
	rm -rf ${tgt}/generated/
	rm -rf ${tgt}/classes/


#
# Directory structure
#
${tgt}/generated/: | ${tgt}/
${tgt}/classes/:   | ${tgt}/


#
# Generate R.java and other resources
#
${tgt}/generated.log: AAPT        := ${AAPT}
${tgt}/generated.log: manifest    := ${manifest}
${tgt}/generated.log: resources   := ${resources}
${tgt}/generated.log: ANDROID_JAR := ${ANDROID_JAR}
${tgt}/generated.log: ${manifest}
${tgt}/generated.log: ${resources}
${tgt}/generated.log: ${resources_files}
${tgt}/generated.log: | ${tgt}/generated/
${tgt}/generated.log: ; \
	${AAPT} package -v -f -m \
		-M ${manifest} \
		--auto-add-overlay \
		$(strip $(foreach r, ${resources}, -S $r)) \
		-I ${ANDROID_JAR} \
		-J $| > $@


#
# Compile the source code including R.java
#
cp := ${tgt}/classes
cp += ${libraries}
cp := $(subst ${SPACE},"${PATHSEP}",$(strip ${cp}))
cp := ${cp}"${PATHSEP}"${ANDROID_JAR}
cp := ${cp}$(and ${CLASSPATH},"${PATHSEP}"${CLASSPATH})
sp := ${tgt}/generated
sp += ${sources}
sp := $(subst ${SPACE},"${PATHSEP}",$(strip ${sp}))
${tgt}/classes.log: JAVAC       := ${JAVAC}
${tgt}/classes.log: JAVAC_FLAGS := ${JAVAC_FLAGS}
${tgt}/classes.log: ANDROID_JAR := ${ANDROID_JAR}
${tgt}/classes.log: cp          := ${cp}
${tgt}/classes.log: sp          := ${sp}
${tgt}/classes.log: ${tgt}/generated.log
${tgt}/classes.log: $(filter %.java, ${sources_files})
${tgt}/classes.log: ${libraries}
${tgt}/classes.log: | ${tgt}/classes/
${tgt}/classes.log: ; \
	${JAVAC} -encoding UTF-8 \
		-bootclasspath ${ANDROID_JAR} \
		-classpath  ${cp} \
		-sourcepath ${sp} \
		${JAVAC_FLAGS} \
		-d $| \
		$(or $(filter %.java, $?), $(filter %.java, $^)) \
		> $@


#
# Generate the main DEX file
#
${tgt}/classes.dex: DEX := ${DEX}
${tgt}/classes.dex: ${tgt}/classes.log
${tgt}/classes.dex: ${libraries}
${tgt}/classes.dex: ; \
	$(call PUSHD, ${@D}) && \
	${DEX} --dex --verbose \
		--output=${@F} \
		classes/ \
		$(call relpath, ${@D}, $(filter %.jar, $^))


#
# Build raw apk
#
${tgt}/apk-raw.apk: AAPT        := ${AAPT}
${tgt}/apk-raw.apk: manifest    := ${manifest}
${tgt}/apk-raw.apk: resources   := ${resources}
${tgt}/apk-raw.apk: ANDROID_JAR := ${ANDROID_JAR}
${tgt}/apk-raw.apk: ${manifest}
${tgt}/apk-raw.apk: ${resources}
${tgt}/apk-raw.apk: ${resources_files}
${tgt}/apk-raw.apk: ${tgt}/classes.dex
${tgt}/apk-raw.apk: ; \
	${AAPT} package -v -f -m \
		-M ${manifest} \
		--auto-add-overlay \
		$(strip $(foreach r, ${resources}, -S $r)) \
		-I ${ANDROID_JAR} \
		-F $@ \
	&& \
	$(call PUSHD, ${@D}) && \
	${AAPT} add -v -f ${@F} classes.dex

#
# Generate default debug.keystore
#
${tgt}/debug.keystore: KEYTOOL := ${KEYTOOL}
${tgt}/debug.keystore:
	${KEYTOOL} -genkey -v \
		-noprompt \
		-keystore $@ \
		-alias android \
		-sigalg MD5withRSA \
		-keyalg RSA \
		-keysize 1024 \
		-storepass android \
		-keypass android \
		-dname "CN=Android Debug,O=Android,C=US"

#
# Sign apk with
#
${tgt}/apk-sign.apk: JARSIGNER := ${JARSIGNER}
${tgt}/apk-sign.apk: ${tgt}/apk-raw.apk
${tgt}/apk-sign.apk: ${tgt}/debug.keystore
${tgt}/apk-sign.apk: ; \
	${JARSIGNER} -verbose \
		-keystore $(word 2, $^) \
		-storepass android \
		-keypass android \
		-sigalg MD5withRSA -digestalg SHA1 \
		-signedjar $@ \
		$(word 1, $^) \
		android

#
# Align apk
#
${tgt}/apk-align.apk: ZIPALIGN := ${ZIPALIGN}
${tgt}/apk-align.apk: ${tgt}/apk-sign.apk
	${ZIPALIGN} -v -f 4 $< $@

#
# Install apk
#

${ptgt}/install: ADB := ${ADB}
${ptgt}/install: ${tgt}/apk-align.apk
${ptgt}/install:
	${ADB} install -r $<

${ptgt}/uninstall: ADB := ${ADB}
${ptgt}/uninstall: package := ${package}
${ptgt}/uninstall:
	${ADB} uninstall ${package}

${ptgt}/install-force: ADB := ${ADB}
${ptgt}/install-force: ${tgt}/apk-align.apk
${ptgt}/install-force: ${ptgt}/uninstall
${ptgt}/install-force:
	${ADB} install -r $<

#
# Run activity
#

define t =
${ptgt}/start-${activity}: ADB := ${ADB}
${ptgt}/start-${activity}: ${ptgt}/install-force
	$${ADB} shell am start -n ${package}/.${activity}
endef
$(foreach activity, ${activities}, $(eval $t))
