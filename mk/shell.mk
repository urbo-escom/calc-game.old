# Windows
ifneq ($(or ${SYSTEMROOT}, ${SystemRoot}),)
ifeq ($(or ${OSTYPE}, ${MSYSTEM}),)
override SHELL_TYPE ?= cmd
else
override SHELL_TYPE ?= sh
endif
override PATHSEP    ?= ;
endif

# Non-Windows + sh
ifndef SYSTEMROOT
override SHELL_TYPE ?= sh
override PATHSEP    ?= :
endif

shell_commands :=
shell_commands += CAT
shell_commands += CP
shell_commands += ECHO
shell_commands += MKDIR
shell_commands += MV
shell_commands += POPD
shell_commands += PUSHD
shell_commands += RM
shell_commands += RMDIR
shell_commands += TOUCH
shell_commands += TRUE
shell_commands += WHICH


ifndef SHELL_TYPE
$(error SHELL_TYPE undefined)
endif


ifndef PATHSEP
$(error PATHSEP undefined)
endif


ifeq (${SHELL_TYPE},sh)
	CAT    = cat $1
	CMDSEP = ;
	CP     = cp $1 $2
	ECHO   = echo $1
	MKDIR  = mkdir $1
	MV     = mv $1 $2
	POPD   = popd
	PUSHD  = pushd $1
	RM     = rm $1
	RMDIR  = rm -rf $1
	STRING = '$1'
	TOUCH  = touch $1
	TRUE   = true
	WHICH  = which $1 2>/dev/null
endif


ifeq (${SHELL_TYPE},cmd)
	CAT    = ($(foreach f, $(subst /,\,$1), type $f &&) echo.>nul)
	CMDSEP = &
	CP     = copy /Y $(subst /,\,$1 $2)
	ECHO   = echo $1
	MKDIR  = mkdir $(subst /,\,$1)
	MV     = move /Y $(subst /,\,$1 $2)
	POPD   = popd
	PUSHD  = pushd $(subst /,\,$1)
	RM     = del $(subst /,\,$1)
	RMDIR  = $(foreach d, $(subst /,\,$1), del /f/s/q $d && rmdir /s/q $d &&) echo.>nul
	STRING = "$1"
	TOUCH  = $(foreach f, $(subst /,\,$1), type nul >> $f && copy /b $f+${COMMA}${COMMA} $f &&) echo.>nul
	TRUE   = echo.true>nul 2>&1
	WHICH  = where $1 2>nul
endif


COMMA := ,
SPACE :=
SPACE +=


ifndef ${$(lastword ${MAKEFILE_LIST})_tgt_dir}
$(lastword ${MAKEFILE_LIST})_tgt_dir := yes
%/:
	$(strip $(if $(wildcard $@.), \
		$(if ${V},@$(call ECHO, Directory $@ exists)), \
	$(call MKDIR, $@)))
endif
