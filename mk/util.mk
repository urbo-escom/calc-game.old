#
# $(call rwildcard, dirs..., patterns...)
#
# Recursively list all  files inside all directories in dirs  that match any
# pattern in patterns. The patterns are  any pattern that is accepted by the
# filter function, in contrast to  the wildcard function that uses different
# patterns,  do not  confuse them.  This  function can  be thought  of as  a
# combination of a recursive wildcard with an additional filter call.
#
# Example: $(call rwildcard, src lib, %.c %.h)
# would produce 'src/foo.c src/foo.h lib/bar/bar.h'
#
rwildcard = $(call rwildcard_iter, $1, $(call _$0, $2))
# create the filter function on the fly
_rwildcard = $(eval _$0 = $$(filter $1, $$1)) _$0


#
# $(call rwildcard_file, dirs...)
# $(call rwildcard_dir, dirs...)
#
# Recursively list either all files  or all directories inside any directory
# in  dirs. These  functions consider  a file  to be  a file  that is  not a
# directory, and a directory the opposite of a file.
#
rwildcard_file = $(call rwildcard_iter, $1, _$0_filter)
rwildcard_dir  = $(call rwildcard_iter, $1, _$0_filter)

_rwildcard_file_filter = $(if $(wildcard $1/.),,$1)
_rwildcard_dir_filter  = $(if $(wildcard $1/.),$1,)


#
# $(call rwildcard_iter, dirs..., func)
#
# Iterate  all files  under all  directories in  dirs invoking  the function
# func. The signature of the function  must be $(call func, file, dir) where
# file is the name of the file and dir is the top, root or superdirectory of
# the file. The result is the stripped concatenation of all function calls.
#
rwildcard_iter = $(strip                    \
	$(foreach dir, $(patsubst %/,%,$1), \
		$(call _$0, ${dir}, $2)     \
	))

_rwildcard_iter =                            \
	$(foreach child, $(wildcard $1/*),   \
		$(call $2, ${child}, ${dir}) \
		$(call $0, ${child}, $2)     \
	)


#
# $(call relpath, base, paths...)
#
# Transform all  paths to be  relative to the given  base. An empty  base is
# equal to a .  base. The base and the paths must be  either all relative or
# all absolure paths. The base and  the paths aren't required to exist, just
# like in the abspath function. Example:
#
#         $(call relpath, a, a a/b ../c b/c)
#
# produces the result '. b ../../c ../b/c'.
#
# This function  doesn't use  CURDIR at  all (CURDIR  can have  spaces which
# messes up GNU Make syntax).
#
# Mixing relative paths  with an absolute base or vice  versa produces wrong
# results. With a relative base, all absolute paths are treated like if they
# were removed  from their root base,  like '/a/b -> a/b';  with an absolute
# base the paths are  treated like if they were prepended  with a root base,
# like 'a/b -> /a/b'.
#
# Producing  a correct  result for  a  mixed case  would require  additional
# information: the  base of the relative  paths or the base  of the relative
# base. With all relative paths or all  absolute paths the base of all paths
# is the same: either the current directory or the root directory.
#
# The algorithm is based on two facts:  we can use abspath reliably, i.e. no
# paths with  spaces, by providing our  own fake root since  abspath doesn't
# require the paths to exist; and the relative path remains the same if both
# its base  and the  path it points  to change their  root, for  example the
# bases /c and  /foo/c both have the  same relative path ../a/b  to /a/b and
# /foo/a/b respectively.
#
relpath = $(strip                                                       \
	$(foreach path, $2,                                             \
		$(foreach fake_root, $(call _$0_fake_root, $1 ${path}), \
			$(call _$0_abs2rel,                             \
				$(abspath ${fake_root}/./$(strip $1)),  \
				$(abspath ${fake_root}/./${path}))      \
		)))

# Helper functions

# create a  root large enough to  resolve any .. directory,  or otherwise we
# will hit a wall when abspath resolves paths of the form /.. to /
_relpath_fake_root_sp :=
_relpath_fake_root_sp +=
_relpath_fake_root = \
	/p/$(subst ${$0_sp},/,$(strip $(foreach p,$(subst /,${$0_sp},$1),p)))

# intermediate  function for  a  more convenient  recursive  call that  also
# removes the junk that could be created by it
_relpath_abs2rel = \
	$(patsubst %/.,%, $(call $0_find,$(strip $1),$(strip $2)))

# the heart of the algorithm: try to go  from the base $1 to the path $2, if
# we haven't arrived yet, try moving up and finding it there; we know we are
# there when $2 is equal to or is a subdirectory of $1.
_relpath_abs2rel_find = \
	$(if $(and $(findstring $1,$2), $(findstring $2,$1)), \
		., \
	$(if $(filter $1/%,$2), \
		$(patsubst $1/%,%,$2), \
	../$(strip $(call $0,$(patsubst %/,%,$(dir $1)),$2)) \
	))
